cmake_minimum_required(VERSION 3.13)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project(interpreter CXX)

file(GLOB_RECURSE SOURCES . src/*.cpp)
add_executable(interpreter ${SOURCES})

target_include_directories(interpreter PUBLIC "./src")

