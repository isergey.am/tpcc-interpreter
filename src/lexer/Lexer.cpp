#include "Lexer.hpp"


Lexer::Lexer(std::istream& input)
	: input_(input) {

}

Token Lexer::NextToken() {
	std::optional<Token> result;
	while (!result) {
		result = TryNextToken();
	}

	return *result;
}

std::optional<Token> Lexer::TryNextToken() {
	if (Peek(1) == EOF) {
		return MakeToken(TokenType::END_OF_FILE, "\0");
	}

	switch (Advance()) {
		case '(': return MakeToken(TokenType::BRACKET_OPEN_ROUND, "(");
		case ')': return MakeToken(TokenType::BRACKET_CLOSE_ROUND, ")");
		case '{': return MakeToken(TokenType::BRACKET_OPEN_CURLY, "{");
		case '}': return MakeToken(TokenType::BRACKET_CLOSE_CURLY, "}");

		case ';': return MakeToken(TokenType::SEMICOLON, ";");
		case ',': return MakeToken(TokenType::COMMA, ",");
		case '.': return MakeToken(TokenType::DOT, ".");

		case '=':
			if (Peek(1) == '=') {
				return MakeToken(TokenType::ASSIGN, "==");
			} else {
				return MakeToken(TokenType::EQ, "=");
			}

		case '<':
			if (Peek(1) == '=') {
				return MakeToken(TokenType::LEQ, "<=");
			} else {
				return MakeToken(TokenType::LT, "<");
			}

		case '>':
			if (Peek(1) == '=') {
				return MakeToken(TokenType::GEQ, ">=");
			} else {
				return MakeToken(TokenType::GT, ">");
			}

		case '!':
			if (Peek(1) == '=') {
				return MakeToken(TokenType::NEQ, "!=");
			} else {
				return MakeToken(TokenType::BANG, "!");
			}

		case '+': return MakeToken(TokenType::PLUS, "+");
		case '-': return MakeToken(TokenType::MINUS, "-");
		case '*': return MakeToken(TokenType::ASTERISK, "*");

		case '/':
			if (Peek(1) == '/') {
				while (Advance() != '\n') {}
				return std::nullopt;
			} else if (Peek(1) == '*') {
				// Skips the /* so that /*/ does not parse as a comment
				Advance(2);
				while (Advance() != '*' || Peek(1) != '/') {}
				// Skips the *
				Advance();
			} else {
				return MakeToken(TokenType::SLASH, "/");
			}

		case '"': {
			Advance();
			std::string lexem;
			while (Advance() != '"') {
				lexem += Peek();
			}
			return MakeToken(TokenType::STRING_LITERAL, lexem);
		}

		case ' ':
		case '\r':
		case '\t': return std::nullopt;
	}

	// Everything from here on might be bugged
	// TODO: unit tests
	if (std::isdigit(Peek())) {
		return ParseDigits();
	}

	if (std::isalpha(Peek()) || Peek() == '_') {
		return ParseLetters();
	}

	// TODO: print an error and continue parsing
	return std::nullopt;
}

Token Lexer::ParseDigits() {
	std::string lexem {Peek()};
	while (std::isdigit(Peek(1))) {
		lexem += Advance();
	}

	return MakeToken(TokenType::NUMBER_LITERAL, lexem);
}

Token Lexer::ParseLetters() {
	std::string lexem {Peek()};
	while (std::isdigit(Peek(1)) || std::isalpha(Peek(1)) || Peek(1) == '_') {
		lexem += Advance();
	}
	// Primes in identifiers are cool
	while (Peek(1) == '\'') {
		lexem += Advance();
	}

	auto iter = keywords_.find(lexem);
	if (iter == keywords_.end()) {
		return MakeToken(TokenType::IDENTIFIER, lexem);
	}

	return MakeToken(iter->second, iter->first);
}

char Lexer::Peek(size_t offset) {
	while (offset >= lookAhead_.size()) {
		char next = 0;
		input_ >> next;
		// Invalidates lookAhead_ iterators, so we use an integer index instead
		lookAhead_ += next;
	}

	return lookAhead_[offset];
}

char Lexer::Advance(size_t count) {
	for (size_t i = 0; i < count && i < lookAhead_.size(); ++i) {
		if (lookAhead_[i] == '\n') {
			++currentLine_;
		}
	}

	if (current_ + count >= lookAhead_.size()) {
		input_.ignore(current_ + count - lookAhead_.size());

		lookAhead_.clear();
		current_ = 0;

		Peek();
	} else {
		current_ += count;
	}

	// Might be dangerous as EOF is not distinguishable from \0
	if (lookAhead_[current_] == EOF) {
		// TODO: Report unexpected EOF and stop parsing
	}

	return lookAhead_[current_];
}

Token Lexer::MakeToken(TokenType type, std::string lexem) {
	return Token {type, lexem, currentLine_};
}

const std::unordered_map<std::string, TokenType> Lexer::keywords_ {
		{"struct", TokenType::STRUCT},
		{"if", TokenType::IF},
		{"else", TokenType::ELSE},
		{"while", TokenType::WHILE},
		{"fun", TokenType::FUN},
		{"nil", TokenType::NIL},
		{"return", TokenType::RETURN},
		{"true", TokenType::TRUE},
		{"false", TokenType::FALSE}
	};
