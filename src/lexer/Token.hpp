#ifndef LEXER_TOKEN_HPP_
#define LEXER_TOKEN_HPP_

#include <cinttypes>
#include <string>

enum class TokenType {
	BRACKET_OPEN_ROUND,
	BRACKET_CLOSE_ROUND,
	BRACKET_OPEN_CURLY,
	BRACKET_CLOSE_CURLY,

	SEMICOLON,
	COMMA,
	DOT,

	ASSIGN,

	EQ, NEQ,
	LT, LEQ,
	GT, GEQ,

	BANG,
	AND, OR,
	TRUE, FALSE,

	PLUS,
	MINUS,
	SLASH,
	ASTERISK,

	STRING_LITERAL,
	NUMBER_LITERAL,

	IDENTIFIER,

	STRUCT, IF, ELSE, WHILE, FUN, NIL, RETURN,

	END_OF_FILE
};

struct Token {
	TokenType type;
	std::string lexem;
	uint64_t line;
};

#endif // LEXER_TOKEN_HPP_
