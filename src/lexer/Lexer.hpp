#ifndef LEXER_LEXER_HPP_
#define LEXER_LEXER_HPP_

#include <iostream>
#include <optional>
#include <string>
#include <string_view>
#include <cctype>
#include <unordered_map>

#include "Token.hpp"

class Lexer {
public:
	Lexer(std::istream& input);

	Token NextToken();

	// Might fail due to parsing errors, comments or space characters
	std::optional<Token> TryNextToken();

private:
	Token ParseDigits();
	Token ParseLetters();

	// TODO: maybe decompose this into a look-aheadable stream
	// this is better than seekg/tellg as we keywords can simply be
	// compared to the lookahead
	char Peek(size_t offset = 0);
	char Advance(size_t count = 1);
	Token MakeToken(TokenType type, std::string lexem);

private:
	std::istream& input_;
	std::string lookAhead_;
	uint64_t current_;
	uint64_t currentLine_ {0};

	static const std::unordered_map<std::string, TokenType> keywords_;
};

#endif // LEXER_LEXER_HPP_
