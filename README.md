# A small concurrent loh-based language interpreter
https://craftinginterpreters.com

Differences from loh (subject to change):

 * Concurrency
 * Strong types
 * No object-oriented features